package dam.androidjoseluna.u3t2implicItintents;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String IMPLICIT_INTENTS = "ImplicitIntents";
    public EditText etUri, etLocation, etText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
    }

    private void setUI(){
        Button btOpenUri, btOpenLocation,btShareText,btNextActivity;

        etUri = findViewById(R.id.etUri);
        etLocation = findViewById(R.id.etLocation);
        etText = findViewById(R.id.etText);

        btOpenUri = findViewById(R.id.btOpenUri);
        btOpenLocation = findViewById(R.id.btOpenLocation);
        btShareText = findViewById(R.id.btShareText);
        btNextActivity = findViewById(R.id.Pasar);

        btOpenUri.setOnClickListener(this);
        btOpenLocation.setOnClickListener(this);
        btShareText.setOnClickListener(this);
        btNextActivity.setOnClickListener(this);

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view){
        switch (view.getId()){
            case R.id.btOpenUri:
                openWebsite(etUri.getText().toString());
                break;
            case R.id.btOpenLocation:
                    openLocation(etLocation.getText().toString());
                break;
            case R.id.btShareText:
                shareText(etText.getText().toString());
                break;
            case R.id.Pasar:
                startActivity(new Intent(MainActivity.this,MoreIntents.class));
                break;
        }
    }

    private void openWebsite(String UrlText){
        Uri webpage = Uri.parse(UrlText);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);

        if (intent.resolveActivity(getPackageManager()) != null){
            startActivity(intent);
        }else{
            Log.d(IMPLICIT_INTENTS, "openWebsite: Can't handle this intent!");
        }
    }

    private void openLocation(String location){
        Uri addressUri = Uri.parse("geo:0,0?q="+location);
        Intent intent = new Intent(Intent.ACTION_VIEW, addressUri);

        if ( intent.resolveActivity(getPackageManager()) != null){
            startActivity(intent);
        }else{
            Log.d("ImplicitIntents","openLocation: Can't handle this intent!");
        }
    }

    private void shareText(String text){
        new ShareCompat.IntentBuilder(this)
                .setChooserTitle(text)
                .setType("text/plain")
                .setText(text)
                .startChooser();
    }

}