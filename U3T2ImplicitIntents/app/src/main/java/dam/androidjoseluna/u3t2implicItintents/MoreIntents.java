package dam.androidjoseluna.u3t2implicItintents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MoreIntents extends AppCompatActivity implements View.OnClickListener {
    EditText mensajes;
    EditText horas;
    EditText minutos;
    EditText segundos;
    EditText mensaje2;
    EditText mensaje3;
    EditText persona;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_intents);
        setUI();
    }

    private void setUI() {
        Button btNextActivity,crearAlarma, creartemp, crearEmail;

        mensajes = findViewById(R.id.mensaje);
        horas = findViewById(R.id.horas);
        minutos = findViewById(R.id.minutos);
        segundos = findViewById(R.id.segundos);
        mensaje2 = findViewById(R.id.mensaje2);
        mensaje3 = findViewById(R.id.mensaje3);
        persona = findViewById(R.id.persona);

        btNextActivity = findViewById(R.id.volver);
        crearAlarma = findViewById(R.id.CrearAlarma);
        creartemp = findViewById(R.id.creartemp);
        crearEmail = findViewById(R.id.crearContacto);

        btNextActivity.setOnClickListener(this);
        crearAlarma.setOnClickListener(this);
        creartemp.setOnClickListener(this);
        crearEmail.setOnClickListener(this);

    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.CrearAlarma:
                createAlarm(mensajes.getText().toString(),Integer.parseInt(horas.getText().toString()),Integer.parseInt(minutos.getText().toString()));
                break;
            case R.id.volver:
                startActivity(new Intent(MoreIntents.this,MainActivity.class));
                break;
            case R.id.creartemp:
                startTimer(mensaje2.getText().toString(),Integer.parseInt(segundos.getText().toString()));
                break;
            case R.id.crearContacto:
                insertContact(persona.getText().toString(),mensaje3.getText().toString());
                break;
        }
    }

    public void createAlarm(String message, int hour, int minutes) {
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM)
                .putExtra(AlarmClock.EXTRA_MESSAGE, message)
                .putExtra(AlarmClock.EXTRA_HOUR, hour)
                .putExtra(AlarmClock.EXTRA_MINUTES, minutes);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void startTimer(String message, int seconds) {
        Intent intent = new Intent(AlarmClock.ACTION_SET_TIMER)
                .putExtra(AlarmClock.EXTRA_MESSAGE, message)
                .putExtra(AlarmClock.EXTRA_LENGTH, seconds)
                .putExtra(AlarmClock.EXTRA_SKIP_UI, true);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void insertContact(String name, String email) {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
        intent.putExtra(ContactsContract.Intents.Insert.NAME, name);
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL, email);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }


}